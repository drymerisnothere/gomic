package main

import (
	"bufio"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

// downloadComics descarga una lista de imágenes y las escribe en un directorio.
// El directorio seguirá el formato `origin/$originName/`. El parámetro `origin`
// es un string que contiene el nombre o "namespace" del comic.
func downloadComics(origin string) {
	var filePath string
	var directories = "./comics/" + strings.ToLower(origin) + "/"

	// Leer un fichero y mostrar el error de haberlo
	fileHandle, err := os.Open("./origin/" + origin + ".txt")
	if err != nil {
		log.Fatal(err)
	}

	// Cerrar el fichero aplazado
	defer fileHandle.Close()

	// Crear un objeto escaner, que se usará para leer el fichero origen y
	// hacer un bucle, siendo cada iteración una linea de este fichero.
	fileScanner := bufio.NewScanner(fileHandle)
	for fileScanner.Scan() {
		// Inizializar y asignar el valor de linea a la variable url
		url := fileScanner.Text()

		// Este if es exclusivo para los libros de colorear, tienen todos el
		// mismo nombre y hay que darles un nombre distinto
		if filepath.Base(url) == "Web.pdf" {
			splitedUrl := strings.Split(url, "/")
			filePath = directories + splitedUrl[4] + "-" +
				splitedUrl[len(splitedUrl)-1]
		} else {
			filePath = directories + filepath.Base(url)
		}

		// Si el fichero ya existe, seguir con el siguiente fichero
		if _, err := os.Stat(filePath); !os.IsNotExist(err) {
			continue
		}

		// Descargar fichero
		response, err := http.Get(url)
		if err != nil {
			// Mostrar el error de haberlo
			log.Print("error downloading " + url)
			log.Print(err)
		} else {
			// Imprimir por pantalla el resultado correcto de la descarga del
			// fichero
			log.Print("successfully downloaded " + url)
		}
		// Cerrar la petición de descarga cuando termine
		defer response.Body.Close()

		// Crear los directorios
		os.MkdirAll(directories, os.ModePerm)

		// Crear el fichero vacío y mostrar el error de haberlo
		file, err := os.Create(filePath)
		if err != nil {
			log.Print("error creating " + filePath)
			log.Print(err)
		}
		// Cerrar el fichero de forma aplazada
		defer file.Close()

		// Escribir el contenido descargado en el fichero y mostrar el error de
		// haberlo
		_, err = io.Copy(file, response.Body)
		if err != nil {
			log.Print("error writing" + filePath)
			log.Print(err)
		}
	}
}

// ls devuelve un array de strings con los comics del origen que recibe.
func ls(origin string) []string {
	// Declarar variable
	var images []string

	// Declarar e instanciar variable con el valor `statc/$origen.html`.
	var path = "comics" + strings.Replace(filepath.Clean(origin), ".html", "",
		-1)

	// Listar ficheros de del directorio `./static/$origin.html` y capturar el
	// error de haberlo
	_images, err := ioutil.ReadDir(path)
	if err != nil {
		log.Print("the path " + path + " doesn't exist")
	}

	// Convertir el array de os.FileInfo a un array de strings
	for _, entry := range _images {
		images = append(images, "/"+path+"/"+entry.Name())
	}

	return images
}

// getOrigins devuelve un array de strings con los origenes detectados.
// Los orígenes se detectan haciendo un ls del directorio de `origins`, en el
// que hay directorios con la extensión `.txt`.
func getOrigins() []string {
	// Declarar variable
	var origins []string

	// Listar ficheros del directorio `./origin/` y captura el error de haberlo
	files, err := ioutil.ReadDir("./origin/")
	if err != nil {
		log.Fatal(err)
	}

	// Iterar sobre los ficheros del directorio anterior, creando un array de
	// strings con el nombre de la ruta completo de los ficheros
	for _, f := range files {
		if f.Name() != "random.txt" {
			origins = append(origins, (strings.Replace(f.Name(), ".txt", "",
				-1)))
		}
	}

	return origins
}

// comicServer sirve el html de los endpoint de los origenes. Según la petición
// que llegue, se renderizan los comics de ese origen. El parámetro `w` es el
// objeto que renderizará el html. El parámetro `r` es la petición de entrada,
// de la que se saca el nombre del origen.
func comicServer(w http.ResponseWriter, r *http.Request) {
	// Declarar array de strings
	var images []string
	var existingOrigins []string
	// Declarar un diccionario de array de strings
	var parameter = make(map[string][]string)

	// Declarar e instanciar con el valor `./templates/$origen`.
	variableTemplate := filepath.Join("./templates",
		filepath.Clean(r.URL.Path)+".html")
	// Declarar e instanciar con el valor `./templates/layout.html`.
	layoutTemplate := filepath.Join("./templates", "layout.html")
	// Coger los origenes
	existingOrigins = getOrigins()
	// Listar los ficheros del origen
	images = ls(filepath.Clean(r.URL.Path))

	// Asignar al diccionario las imagenes y los origenes
	parameter["images"] = images
	parameter["origins"] = existingOrigins

	// Pasar la función ToLower a los templates
	funcMap := template.FuncMap{
		"ToLower": strings.ToLower,
	}

	// Declarar, instanciar y ejecutar el objeto template
	tmpl, _ := template.New("random").Funcs(funcMap).ParseFiles(layoutTemplate,
		variableTemplate)
	// Renderizar el html
	tmpl.ExecuteTemplate(w, "layout", parameter)
}

// random sirve el html del endpoint /random/. Muestra un comic aleatorio de los
// origenes definidos en el fichero random.txt. El parámetro `w` es el objeto
// que renderizará el html. El parámetro `r` es la petición de entrada, de la
// que se saca el nombre del origen.
func random(w http.ResponseWriter, r *http.Request) {
	// Declarar variables
	var usableOrigin []string
	var usedOrigin string
	var existingOrigins []string
	var parameter = make(map[string][]string)

	// Definir los templates
	layoutTemplate := "./templates/layout.html"
	randomTemplate := "./templates/random.html"

	// Leer un fichero en formato bytes y mostrar el error de haberlo
	fileHandle, err := os.Open("./origin/random.txt")
	if err != nil {
		log.Fatal(err)
	}

	// Cerrar el fichero
	defer fileHandle.Close()

	// Crear un objeto escaner, que se usará para leer el fichero origen y
	// hacer un bucle, siendo cada iteración una linea de este fichero.
	fileScanner := bufio.NewScanner(fileHandle)
	for fileScanner.Scan() {
		// Inizializar y asignar el valor de linea a la variable usableOrigin
		usableOrigin = append(usableOrigin, fileScanner.Text())
	}
	// Definir aleatoriamente que origen se servirá
	usedOrigin = usableOrigin[rand.Intn(len(usableOrigin))]

	// Coger los origenes
	existingOrigins = getOrigins()
	// Listar los ficheros del origen
	images := ls("/" + usedOrigin)
	// Sortear que imagen mostrar
	randomImage := rand.Intn(len(images))
	// Asignar al diccionario la imagen y los origenes
	parameter["images"] = append(parameter["images"], images[randomImage])
	parameter["origins"] = existingOrigins

	// Pasar la función ToLower a los templates
	funcMap := template.FuncMap{
		"ToLower": strings.ToLower,
	}

	// Declarar, instanciar y ejecutar el objeto template
	tmpl, _ := template.New("random").Funcs(funcMap).ParseFiles(layoutTemplate,
		randomTemplate)
	// Renderizar el html
	tmpl.ExecuteTemplate(w, "layout", parameter)
}

func main() {
	// Declarar variable
	var origins []string
	var port = ":8080"

	// Añadir el directorio `static` como directorio disponible en el servidor
	// web
	fs := http.FileServer(http.Dir("./static/"))
	// Añadir el directorio `comics` como directorio disponible en el servidor
	// web
	cm := http.FileServer(http.Dir("./comics/"))
	// Asociar el endpoint `/static/` con el directorio `static`
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	// Asociar el endpoint `/comics/` con el directorio `comics`
	http.Handle("/comics/", http.StripPrefix("/comics/", cm))

	// Instanciar variable con los origenes
	origins = getOrigins()
	for _, origin := range origins {
		// Descargar los comics de cada origen
		downloadComics(origin)
		// Asignar un endpoint a cada origen
		http.HandleFunc("/"+strings.ToLower(origin)+"/", comicServer)
	}
	// Hacer que random sea el endpoint por defecto
	http.HandleFunc("/", random)
	// Imprimir por pantalla la dirección del servidor web
	log.Println("Starting webserver at http://localhost" + port)
	// Crear y servir el servidor
	http.ListenAndServe(port, nil)
}
