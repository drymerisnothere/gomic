#!/usr/bin/env bash

set -e

if [ -n "$(go fmt gomic.go)" ]
then
    return 1
fi
return 0
