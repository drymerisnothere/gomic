#!/usr/bin/env bash

apk add -U git > /dev/null
cd /gomic
CGO_ENABLED=0 GOOS=linux go build -i -a -ldflags '-extldflags "-static"' -o gomic
