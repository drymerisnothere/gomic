FROM golang:1.12.6-alpine3.9 as builder
COPY gomic.go /go/gomic.go
RUN apk add -U git
RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o gomic .

FROM scratch as production
COPY --from=builder /go/gomic /gomic/gomic
COPY templates /gomic/templates/
COPY static /gomic/static/
COPY origin /gomic/origin/
WORKDIR /gomic
ENTRYPOINT ["/gomic/gomic"]
