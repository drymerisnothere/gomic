# gomic

Este proyecto nace como un ejercicio para aprender un poco de go. Esencialmente
descarga imágenes y las muestra en un servidor web. La idea es recopilar comics
que tengan que ver con diferentes temas tecnológicos y tenerlos en un solo sitio.

Hay que crear unos origenes, que vienen a ser ficheros con extensión `.txt` que
contengan una lista de imágenes. Cada origen es un recopilatorio de comics de
cierta temática tecnológica común. Por ejemplo, el origen `Julia Evans`
contiene muchos de los comics que ella ha hecho y publicado en twitter (aunque
todos se pueden comprar en [su página web](https://wizardzines.com/)).

![Ejemplo](./img/comics.png)

## Compilar
Solo hace falta tener docker:

``` bash
make build
```

## Crear imágen docker

``` bash
make docker-build
```

## Usar

``` bash
./gomic
# O con docker
docker run -ti -v `pwd`/comics/:/gomic/comics -v `pwd`/origin/:/gomic/origin -v /etc/ssl:/etc/ssl ./gomic
```
