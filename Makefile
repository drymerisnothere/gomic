.PHONY: help

help:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo "  clean                       remove the go binary"
	@echo "  build                       build the binary"
	@echo "  docker-build                build the docker image"
	@echo "  push                        push the docker image"
	@echo "Check the Makefile to know exactly what each target is doing."

test:
	@echo "Checking format"
	@sh scripts/check-format.sh

clean:
	@echo "Deleting binary"
	@rm -f gomic

build:
	@echo "Creating static binary"
	@docker run -i -v `pwd`:/gomic golang:1.11.5-alpine3.9 sh /gomic/scripts/build.sh

docker-build:
	docker build -t r.daemons.it/gomic:latest --target production .

docker-push:
	docker push r.daemons.it/gomic:latest

all: clean build docker-build
